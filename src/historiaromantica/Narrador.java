package historiaromantica;

public class Narrador {
    
    void InicioHistoria(){
        System.out.println("Habia una vez en un pais, una mujer Llamada Josefina.");
        System.out.println("Ella era una gran estudiante en su universidad pero sentia que le faltaba algo");
        System.out.println("Ella queria un novio, no como los que tuvo, ella queria un novio diferente y penso: ");
    }
    void InicioHistoria1(){
        System.out.println("Pero ella no sabia que quiera en realidad, el amor es raro pues el chico que le gustaba no le ponia cuidado");
    }
    void InicioHistoria2(){
        System.out.println("Dijo ella");
    }
     void InicioHistoria3(){
        System.out.println("Pero Josefina lo que no sabia era que tenia un pequeño enamorado, su nombre era Sneyder, chico timido de la universidad");
    }
    void InicioHistoria4(){
        System.out.println("Dijo el");
    }
    void InicioHistoria5(){
        System.out.println("Cuando se comenzo ha acercar, ella lo volteo a mirar y se fue rapido");
    }
    void InicioHistoria6(){
        System.out.println("Cuando el vio que se estaba alejando comenzo a sentirse mal, freno y su sonrisa se apago");
    }
    void InicioHistoria7(){
        System.out.println("En el fondo se escucho a Brayan Alberto, el mejor amigo de Sneyder");
    }
    void InicioHistoria8(){
        System.out.println("Sneyder tenia un puñal con el que queria hacerle daño a Bayron");
    }
    void InicioHistoria9(){
        System.out.println("Le dijo Brayan Alberto a su amigo.");
        System.out.println("En el salon de la clase de pedagogica infantil, Sneyder miro que estaba entrando Josefina");
        System.out.println("El estaba mirandola, hasta el momento que ella lo vio y el desvio la mirada");
    }
    void InicioHistoria10(){
        System.out.println("Entro el profesor y les dijo: Les toca trabajar en pareja, las pareja las hago yo");
        System.out.println("Josefina por dentro decia: ");
    }
    void InicioHistoria11(){
        System.out.println("y Sneyder por dentro decia: ");
    }
    void InicioHistoria12(){
        System.out.println("El profesor llamo a todos y quedaron Josefina y Sneyder juntos");
        System.out.println("Pero habia un problema, un estudiante llego tarde y este no tenia grupo");
        System.out.println("Joven ¿como se llama usted? Pregunto el profesor");
    }
    void InicioHistoria13(){
        System.out.println("Queda asignado al ultimo grupo señor Bayron, al de Josefina y Sneyder");
        System.out.println("Todos quedaron pasmados");
        System.out.println("Josefina penso");
        
    }
    void InicioHistoria14(){
        System.out.println("Sneyder se veia triste despues de una sonrisa y pensaba");
    }
    void InicioHistoria15(){
        System.out.println("Bayron los miro y dijo:");
    }
    void InicioHistoria16(){
        System.out.println("El profesor contesto: Aparte de llegar tarde viene a elegir sus compañeros, pues NO!");
        System.out.println("Se miraron todos y se sentaron y el profesor dijo");
        System.out.println("Para mañana me tienen que traer una cartelera de como educar niños");
        System.out.println("No se les olvide que el que no trabaje lo sacan del grupo");
    }
    void InicioHistoria17(){
        System.out.println("Dijo ella con una cara de enamorada y sin quitarle la mirada a Bayron");
        System.out.println("Entonces se escucho de la boca de Bayron");
    }
    void InicioHistoria18(){
        System.out.println("Y Sneyder contesto:");
    }
    void InicioHistoria19(){
        System.out.println("En la tarde Josefina no sabia que ponerse");
    }
    void InicioHistoria20(){
        System.out.println("Cuando sono el timbre de la casa, ella salio corriendo a abrir la puerta ");
        System.out.println("Ella penso que era Bayron, pero en realidad era Sneyder");
        System.out.println("Se veia radiante, olia a perfume nuevo y estaba recien peluqueado");
        System.out.println("Ella lo miro y le dijo: ");
    }
    void InicioHistoria21(){
        System.out.println("Entro la miro de arriba para abajo y le dijo");
    }
    void InicioHistoria22(){
        System.out.println("Ella sonrio y le dijo");
    }
    void InicioHistoria23(){
        System.out.println("Al pasar la tarde, Bayron no llegaba y Sneyder estaba estresado y Josefina no sabia que pasaba");
        System.out.println("Sneyder sabia que Bayron no llegaria y Josefina todavia tenia fe");
        System.out.println("Entonces de la boca de Sneyder salio");
    }
    void InicioHistoria24(){
        System.out.println("Con cara de desilucion ella asintio y se colocaron a hacer el trabajo");
        System.out.println("Al pasar la tarde, Josefina comenzo a sonreir de todo lo que le decia Sneyder");
        System.out.println("Ella comenzo a pensar");
    }
    void InicioHistoria25(){
        System.out.println("Al siguiente dia llegaron al salon y Bayron aparecio y dijo");
    }
    void InicioHistoria26(){
        System.out.println("Sneyder lo miro con rabia y le dijo");
    }
    void InicioHistoria27(){
        System.out.println("Entonces Bayron dijo:");
    }
    void InicioHistoria28(){
        System.out.println("Josefina y Sneyder se asustaron");
        System.out.println("Cuando menos pensaron Bayron mando la puñalada directo a Josefina");
        System.out.println("De forma heroica Sneyder se atraveso y protegio a Josefina");
        System.out.println("Le habia herido el brazo y Sneyder grito y le dijo");
    }
    void InicioHistoria29(){
        System.out.println("Sneyder desenfundo su navaja y se abalanzo sobre Bayron");
        System.out.println("Josefina esta impactada y gritaba que pararan");
        System.out.println("En un momento se metio Brayan Alberto y le dio un puño a Bayron");
        System.out.println("Y llego el profesor y los separo a todos");
        System.out.println("Josefina se fue con Sneyder a la enfermeria rapidamente");
    }
    void InicioHistoria30(){
        System.out.println("Sneyder sonreia porque Josefina le estaba poniendo cuidado y estaba angustiada");
        
    }
    void InicioHistoria31(){
        System.out.println("Todo estaba por los cielos, Bayron estaba herido y llego una ambulacia por el");
        System.out.println("Entonces lo montaron en ella pero Brayan Alberto dijo");
    }
    void InicioHistoria32(){
        System.out.println("Se monto en la ambulancia cuando nadie lo vio y se robo la ambulancia");
        System.out.println("El acelero y se la llevo muy lejos, cuando llego a una bahia");
        System.out.println("Busco un ladrillo y abrio la puerta y grito");
        
    }
    void InicioHistoria33(){
        System.out.println("Bayron escucho eso y comenzo a gritar");
        
    }
    void InicioHistoria34(){
        System.out.println("Se escucho en el fondo una voz familiar que decia: ");
        System.out.println("Se lo merece porque no sabia tratar a una mujer");
        System.out.println("Cuando Bayron miro quien era se sorprendio y dijo: ");
        
    }
    void InicioHistoria35(){
        System.out.println("Arranco la ambulancia y Bayron murio.");
        System.out.println("Sneyder y Josefina se enamoraron y extrañamente Trixi y Brayan Alberto eran novios");
        System.out.println("Al final Sneyder le propone matrimonio a Josefina y se casan.");
        System.out.println("Josefina tiene un hijo llamado Stewart que se enamora de la hija de Brayan Alberto y Trixi llamada Anacleta");
        System.out.println("¡Fin!");
    }
     
    
}
